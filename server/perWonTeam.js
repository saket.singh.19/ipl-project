function matchPerWonTeam(matches){
    let result2 = {}
    for(let index of matches){
  
        if(result2[index.winner]){
            if(result2[index.winner][index.season]){
                result2[index.winner][index.season] += 1
            }else{
                result2[index.winner][index.season] = 1
            }
        }else{
            result2[index.winner] = {}
            result2[index.winner][index.season] = 1
        } 
    }
    
    return result2;
  }
  module.exports = matchPerWonTeam;