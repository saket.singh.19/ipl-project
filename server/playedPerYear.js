function matchesPlayedPerYear(matches) {
    let result = {};
    for (let index of matches) {
      let season = index.season;
      if (result[season]) {
        result[season] += 1;
      } else {
        result[season] = 1;
      }
    }
    return result;
  }
  module.exports =matchesPlayedPerYear;