function TopEconomicplayers2015(matchData, deliveryData) {
    let arr2 = [];
    for(let match of matchData) {
        if(match.season === "2015") {
            arr2.push(match.id); 
        }
    }

    let result4 = {};
    for(let delivery of deliveryData) {
        if(arr2.includes(delivery.match_id)) {
            if(result4[delivery.bowler] === undefined) {
                result4[delivery.bowler] = {
                    runs: 0,
                    deliveries: 0
             }; 
            }
            result4[delivery.bowler]['runs'] += +delivery.total_runs;
            result4[delivery.bowler]['deliveries']++;
        }
    }
    

    for(let bowler in result4) { 
         result4[bowler]['overs'] = parseFloat(result4[bowler]['deliveries'] / 6).toFixed(2); 
         result4[bowler]['economy'] = parseFloat(result4[bowler]['runs'] / result4[bowler]['overs']).toFixed(2); 
    }
    
    let top10EconomicB = Object.keys(result4)
                                    .sort(function(a, b){
                                    return +result4[a]['economy'] - +result4[b]['economy'];
                                    })
                                    .slice(0, 10)
                                    .reduce(function(top10List, bowler){
                                        top10List[bowler] = result4[bowler]['economy'];
                                        return top10List;
                                    }, {});

    return top10EconomicB;                                     
}
module.exports = TopEconomicplayers2015;