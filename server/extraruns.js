function ExtraRuns2016(matchData, deliveryData) {
    let arr = []; 
    for(let match of matchData) {
        if(match.season === "2016") {
            arr.push(match.id); 
        }
    }

    let result3 = {};    
    for(let delivery of deliveryData) {
        if(arr.includes(delivery.match_id)) {
            if(!result3[delivery.bowling_team]) {
                result3[delivery.bowling_team] = 0;
            }
            result3[delivery.bowling_team] += +delivery.extra_runs; 
        }
    }
    return result3; 
}

module.exports = ExtraRuns2016;